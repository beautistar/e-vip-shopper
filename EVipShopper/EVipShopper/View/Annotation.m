//
//  Annotation.m
//  EVipShopper
//
//  Created by AOC on 09/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation
@synthesize coordinate , title, subtitle;

- (id) initWithCoordinate:(CLLocationCoordinate2D) _coordinate {
    
    self = [super init];
    
    if(self != nil) {
        coordinate = _coordinate;
    }
    
    return self;
}

@end
