//
//  MyPlaceCell.h
//  EVipShopper
//
//  Created by AOC on 05/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPlaceCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvPlace;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceBounse;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;

@end
