//
//  ReceiptCell.h
//  EVipShopper
//
//  Created by AOC on 06/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvPicture;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@end
