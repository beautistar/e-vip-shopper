//
//  BaseViewController.m
//  EVipShopper
//
//  Created by AOC on 03/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "BaseViewController.h"
#import "MBProgressHUD.h"

@interface BaseViewController () {
    
    MBProgressHUD *_hud;
}

@end
@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self registerNotification];
    
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self unregisterNotification];
}


- (void) registerNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void) unregisterNotification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification object:nil];
}

- (void) keyboardWillShown:(NSNotification *) notification {
    
    NSDictionary *keyboardInfo = [notification userInfo];
    
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyBoardFrame = [keyboardFrameBegin CGRectValue];
    float offsetY = keyBoardFrame.size.height - [self getOffsetYWhenShowKeybarod];
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -offsetY;
        self.view.frame = f;
    }];
}

- (void) keyboardWillBeHidden:(NSNotification *) notification {
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}



#pragma mark - MBProgressHUD

- (void) showLoadingViewWithTitle:(NSString *) title;
{

    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the hud to display with a color
    _hud.color = [UIColor colorWithRed:2/255.0 green:136/255.0 blue:209/255.0 alpha:0.0];

    _hud.labelText = title;

}

- (void) showLoadingView {
    
    [self showLoadingViewWithTitle:nil];
}

- (void) hideLoadingView {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    _hud = nil;
}

- (void) hideLoadingView : (NSTimeInterval) delay {
    [_hud hide:YES afterDelay:delay];
    _hud = nil;
}

#pragma mark - Show Alert

- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative {
    
    NSDictionary *pinkBoldAttribtes = @{NSForegroundColorAttributeName :[UIColor colorWithRed:2/255.0 green:136/255.0 blue:209/255.0 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0]};
    
    NSMutableAttributedString *attributedTitle;
    
    if (title != nil) {
        attributedTitle = [[NSMutableAttributedString alloc] initWithString:title];
        [attributedTitle addAttributes:pinkBoldAttribtes range:NSMakeRange(0, title.length)];
    }
    
    
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:message];
    [attributedMessage addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:16.0] range:NSMakeRange(0, message.length)];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if(title != nil) {
        [alert setValue:attributedTitle forKey:@"attributedTitle"];
    }
    
    [alert setValue:attributedMessage forKey:@"attributedMessage"];
    
    if(strPositivie != nil) {
        UIAlertAction * yesButton = [UIAlertAction
                                     actionWithTitle:strPositivie
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Handel your yes please button action here
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
        
        [alert addAction:yesButton];
    }
    
    if(strNegative != nil) {
        UIAlertAction * noButton = [UIAlertAction
                                    actionWithTitle:strNegative
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
        
        [alert addAction:noButton];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
    //    alert.view.tintColor = [UIColor darkGrayColor];
    //    alert.view.backgroundColor = [UIColor colorWithRed:71/255.0 green:74/255.0 blue:85/255.0 alpha:1.0];
}

#pragma mark - Button actions

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat) getOffsetYWhenShowKeybarod
{
    return 0;
}

@end
