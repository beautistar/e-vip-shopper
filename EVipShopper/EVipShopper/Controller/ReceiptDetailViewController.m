//
//  ReceiptDetailViewController.m
//  EVipShopper
//
//  Created by AOC on 09/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ReceiptDetailViewController.h"

@interface ReceiptDetailViewController() {
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imvProfile;

@end

@implementation ReceiptDetailViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
}


- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    //hide top bar
    [self.navigationController setNavigationBarHidden:YES];
    [self.tabBarController.tabBar setHidden:YES];
    self.tabBarController.navigationController.navigationBar.hidden = YES;
    
    //rounding profile image
    _imvProfile.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

- (IBAction)backAction:(id)sender {

    [self.navigationController popViewControllerAnimated:YES];

}

@end
