//
//  PlaceDetailViewController.m
//  EVipShopper
//
//  Created by AOC on 06/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "PlaceDetailViewController.h"
#import "UIViewMYPop.h"

@interface PlaceDetailViewController() <UITextViewDelegate> {
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imvBg;
@property (weak, nonatomic) IBOutlet UIScrollView *scvDetail;

@property (weak, nonatomic) IBOutlet UIImageView *imvProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnBackVPosition;
@property (weak, nonatomic) IBOutlet UISegmentedControl *smtState;
@property (weak, nonatomic) IBOutlet UIView *vFeedbackPop;
@property (weak, nonatomic) IBOutlet UITextView *tvContent;


@end

@implementation PlaceDetailViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self initView];
}

- (void) initView {
    
    //_popViewVConstrain.constant = -175;
    
    _vFeedbackPop.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _tvContent.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _smtState.selectedSegmentIndex = -1;
    
    _imvProfile.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self.navigationController setNavigationBarHidden:YES];
    [self.tabBarController.tabBar setHidden:YES];
    self.tabBarController.navigationController.navigationBar.hidden = YES;
    
    _imvProfile.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    UIColor *selectedColor = [UIColor colorWithRed: 22/255.0 green:146/255.0 blue:50/255.0 alpha:1.0];
    UIColor *deselectedColor = [UIColor lightGrayColor];
    
    for (UIControl *subview in [_smtState subviews]) {
        if ([subview isSelected])
        {
            [subview setTintColor:selectedColor];
            NSLog(@"selected index %d", (int)_smtState.selectedSegmentIndex);
        }
        else
        {
            [subview setTintColor:deselectedColor];
            NSLog(@"selected index %d", (int)_smtState.selectedSegmentIndex);
        }
    }
}

- (IBAction)showMapAction:(id)sender {
    
}

- (IBAction)leaveFeedbackAction:(id)sender {
    
    
    [_vFeedbackPop setHidden:NO];
    
    [_vFeedbackPop showWithAnimationType:MYPopAnimationTypeCover];
    
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)stateChangeAction:(id)sender {
    
    UIColor *selectedColor = [UIColor colorWithRed: 22/255.0 green:146/255.0 blue:50/255.0 alpha:1.0];
    UIColor *deselectedColor = [UIColor lightGrayColor];
    
    for (UIControl *subview in [_smtState subviews]) {
        if ([subview isSelected])
        {
            [subview setTintColor:selectedColor];
            NSLog(@"selected index %d", (int)_smtState.selectedSegmentIndex);
        }
        else
        {
            [subview setTintColor:deselectedColor];
            NSLog(@"selected index %d", (int)_smtState.selectedSegmentIndex);
        }
    }
}

- (IBAction)cancelFeedbackAction:(id)sender {
    
    [_vFeedbackPop dismiss];
    
    [_vFeedbackPop endEditing:YES];
}

- (IBAction)okFeedbackAction:(id)sender {
    
    [_vFeedbackPop dismiss];
    
    [_vFeedbackPop endEditing:YES];
    
    [self.view endEditing:YES];
    
}

- (IBAction)callAction:(id)sender {
    
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"15643313982"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}


#pragma mark - textview delegate

- (void) textViewDidBeginEditing:(UITextView *)textView {
    
    CGRect frame = _vFeedbackPop.frame;
    frame.origin.y = _vFeedbackPop.frame.origin.y - 80;
    frame.origin.x = _vFeedbackPop.frame.origin.x;
    _vFeedbackPop.frame = frame;
}


- (IBAction)didTap:(id)sender {    
    
    CGRect frame = _vFeedbackPop.frame;
    frame.origin.y = _vFeedbackPop.frame.origin.y + 80;
    frame.origin.x = _vFeedbackPop.frame.origin.x;
    _vFeedbackPop.frame = frame;
    
    [self.vFeedbackPop endEditing:YES];
}

@end
