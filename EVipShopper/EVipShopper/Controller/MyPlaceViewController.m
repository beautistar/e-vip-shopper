//
//  MyplaceViewController.m
//  EVipShopper
//
//  Created by AOC on 04/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "MyPlaceViewController.h"
#import "Const.h"
#import "MyPlaceCell.h"

@interface MyPlaceViewController() <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>{
    
    NSArray *title;
}
@property (weak, nonatomic) IBOutlet UICollectionView *clvMyPlace;

@end

@implementation MyPlaceViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];    
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self initView];
    
}

- (void) initView {
    
    [self.tabBarController setTitle:@"My Places"];
    [self.navigationController setNavigationBarHidden:NO];
    [self.tabBarController.tabBar setHidden:NO];
    self.tabBarController.navigationController.navigationBar.hidden = NO;
}

#pragma mark - 
#pragma mark - collection view delegate

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 1;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.clvMyPlace.frame.size.width / 2 - 5 , self.clvMyPlace.frame.size.width / 2 - 5);
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MyPlaceCell *cell = (MyPlaceCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"MyPlaceCell" forIndexPath:indexPath];
    
    return cell;
    
    
}




@end
