//
//  MyCodeViewController.m
//  EVipShopper
//
//  Created by AOC on 04/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "MyCodeViewController.h"
#import "UIViewMyPop.h"

@interface MyCodeViewController ()
@property (weak, nonatomic) IBOutlet UIView *vPopCode;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vCodeVConstrain;


@end

@implementation MyCodeViewController

bool isShown = false;

- (void) viewDidLoad {
    
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];

    [self.tabBarController setTitle:@"My QR Code"];
    
    _vPopCode.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self initView];
    
}

- (void) initView {
    

}


- (IBAction)showCodeAction:(id)sender {
    
    [_vPopCode setHidden:NO];
    
    [_vPopCode showWithAnimationType:MYPopAnimationTypeCover];
    
}

@end
