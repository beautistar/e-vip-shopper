//
//  TermViewController.m
//  EVipShopper
//
//  Created by AOC on 03/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "TermViewController.h"

@implementation TermViewController

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
}
- (IBAction)closeAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
