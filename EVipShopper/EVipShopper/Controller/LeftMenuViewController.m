//
//  LeftMenuViewController.m
//  EVipShopper
//
//  Created by AOC on 04/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "Const.h"
#import "MainTabViewController.h"

@interface LeftMenuViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblMenu;

@end

@implementation LeftMenuViewController

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
            
        case 3:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MyReceiptsViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 4:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"NotificationsViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 5:
            
            [self gotoProfile];
            break;
        case 6:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        
        default:
            
            MENUINDEX = (int) indexPath.row;

            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MainTabViewController"]] animated:YES];
            
//            [self.sideMenuViewController setContentViewController:(MainTabViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"MainTabViewController"] animated: YES];

            
            [self.sideMenuViewController hideMenuViewController];
            break;

    }
}

- (IBAction)profileTapAction:(id)sender {

    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"My profile" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self gotoProfile];
    }]];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Sign out" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self signout];
    }]];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Remove my account" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        [self removeAccount];
    }]];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];

    actionSheet.view.tintColor = [UIColor colorWithRed:0/255.0f green:122/255.0f blue:255/255.0f alpha:1.0];
   
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void) gotoProfile {
    
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileViewController"]]
                                                 animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

- (void) signout {
    
    UINavigationController *loginNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:loginNav];
    
}

- (void) removeAccount {
    
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.tblMenu.frame.size.height / 7 ;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    
    NSArray *titles = @[@"My places", @"My QR code", @"Scan", @"My receipts", @"Notifications", @"My profile", @"Help"];
    NSArray *images = @[@"favorite_white", @"qr-code_white", @"photo-camera_white", @"receipt_white", @"notification_white", @"male_white", @"question_white"];
    cell.textLabel.text = titles[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
    
    return cell;
}


@end
