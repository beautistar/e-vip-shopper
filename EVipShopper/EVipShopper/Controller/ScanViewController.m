//
//  ScanViewController.m
//  EVipShopper
//
//  Created by AOC on 04/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ScanViewController.h"
#import "QRCodeReaderViewController.h"
#import "QRCodeReaderDelegate.h"
#import "QRCodeReader.h"

@interface ScanViewController() <QRCodeReaderDelegate, UIAlertViewDelegate>

@end

@implementation ScanViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.tabBarController setTitle:@"Scan"];
    
    [self QRScannerView];
    
}

- (IBAction)openScanViewAction:(id)sender {
    
//    [self QRScannerView];
    
}

- (void) QRScannerView {
    
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
        static QRCodeReaderViewController *vc = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            vc                   = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:YES showTorchButton:YES];
          
            //vc.modalPresentationStyle = UIModalPresentationFormSheet;
        });
        vc.delegate = self;
        
        [vc setCompletionWithBlock:^(NSString *resultAsString) {
            NSLog(@"Completion with result: %@", resultAsString);
        }];
        
        [self.navigationController pushViewController:vc animated:NO];
        //[self presentViewController:vc animated:YES completion:NULL];
    }
    else {
        
        [self showAlertDialog:@"Error" message:@"Reader not suppported by the current device" positive:@"OK" negative:nil];
        
    }
}

#pragma mark - QRCodeReader Delegate Methods

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    [reader stopScanning];
    
    [self showAlertDialog:@"E_Vip shopper" message:result positive:@"OK" negative:nil];
    [self.navigationController popViewControllerAnimated:NO];
    
//    [self dismissViewControllerAnimated:YES completion:^{
//        
//        [self showAlertDialog:@"E_Vip shopper" message:result positive:@"OK" negative:nil];
    
//        NSArray *strArr = [result componentsSeparatedByString:@"\n"];
        
//        if (strArr.count != 4) {
//            
//            UIAlertController * alert = [UIAlertController
//                                         alertControllerWithTitle:@"Error"
//                                         message:@"Invalid QR code!"
//                                         preferredStyle:UIAlertControllerStyleAlert];
//            
//            //Add Buttons
//            
//            UIAlertAction* okButton = [UIAlertAction
//                                       actionWithTitle:@"Yes"
//                                       style:UIAlertActionStyleDefault
//                                       handler:^(UIAlertAction * action) {
//                                           //Handle your yes please button action here
//                                           //[self okAction];
//                                       }];
//            
//            [alert addAction:okButton];
//            
//            
//            [self presentViewController:alert animated:YES completion:nil];
//            
//            return ;
//        }
//    }];
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    //[self dismissViewControllerAnimated:YES completion:NULL];
    //[self.navigationController popViewControllerAnimated:YES];
    
    [self.navigationController popViewControllerAnimated:NO];
    
}


@end
