//
//  MyProfileViewController.m
//  EVipShopper
//


#import "MyProfileViewController.h"
#import <ActionSheetPicker_3_0/ActionSheetPicker.h>


@interface MyProfileViewController() <UITextFieldDelegate> {
    
    int gender;
    //NSMutableArray * tagList;
}


@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *imvProfile;
@property (weak, nonatomic) IBOutlet UISegmentedControl *smtGender;
@property (weak, nonatomic) IBOutlet UITextField *tfBirth;

@end

@implementation MyProfileViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    gender = 0;
    //tagList = [NSMutableArray arrayWithObjects:@"first", @"second", @"third", nil];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self initView];
}

- (void) initView {
    
    _bgView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _imvProfile.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    UIColor *selectedColor = [UIColor colorWithRed: 22/255.0 green:146/255.0 blue:50/255.0 alpha:1.0];
    UIColor *deselectedColor = [UIColor lightGrayColor];
        
    [_smtGender setSelectedSegmentIndex:gender];
    
    for (UIControl *subview in [_smtGender subviews]) {
        if ([subview isSelected])
        {
            
            [subview setTintColor:selectedColor];
            NSLog(@"selected index %d", (int)_smtGender.selectedSegmentIndex);
        }
        else
        {
            [subview setTintColor:deselectedColor];
            NSLog(@"selected index %d", (int)_smtGender.selectedSegmentIndex);
        }
    }
}

- (IBAction)genderChangeAction:(id)sender {
    
    UIColor *selectedColor = [UIColor colorWithRed: 22/255.0 green:146/255.0 blue:50/255.0 alpha:1.0];
    UIColor *deselectedColor = [UIColor lightGrayColor];
    
    for (UIControl *subview in [_smtGender subviews]) {
        if ([subview isSelected])
        {
            [subview setTintColor:selectedColor];
            NSLog(@"selected index %d", (int)_smtGender.selectedSegmentIndex);
        }
        else
        {
            [subview setTintColor:deselectedColor];
            NSLog(@"selected index %d", (int)_smtGender.selectedSegmentIndex);
        }
    }
}


#pragma mark-
#pragma mark - textfield delegate

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM dd yyyy";
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    [datePicker setDate:date];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    textField.inputView = datePicker;
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    
    _tfBirth.text = [NSString stringWithFormat:@"%d/%d/%d", (int)[components year], (int)[components month], (int)[components day]];

    
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];

}

- (void) datePickerValueChanged:(UIDatePicker *) sender {
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:sender.date];
    
    _tfBirth.text = [NSString stringWithFormat:@"%d/%d/%d", (int)[components year], (int)[components month], (int)[components day]];


}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

@end
