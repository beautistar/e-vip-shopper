//
//  MapViewController.m
//  EVipShopper
//
//  Created by AOC on 09/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>
//#import "Annotation.h"
#import "Const.h"

@interface MapViewController() <MKMapViewDelegate, CLLocationManagerDelegate> {
    
    
}


@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;

@end


@implementation MapViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [_mapView setDelegate:self];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    #ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
    #endif
    [self.locationManager startUpdatingLocation];
    
    _mapView.showsUserLocation = YES;
    [_mapView setMapType:MKMapTypeStandard];
    [_mapView setZoomEnabled:YES];
    [_mapView setScrollEnabled:YES];
    
    
    [_imvPhoto setImage:[UIImage imageNamed:@"coffee"]];
    
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    NSLog(@"%@", [self deviceLocation]);
    
    //View Area
    MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = self.locationManager.location.coordinate.latitude;
    region.center.longitude = self.locationManager.location.coordinate.longitude;
    region.span.longitudeDelta = 0.005f;
    region.span.longitudeDelta = 0.005f;
    [_mapView setRegion:region animated:YES];
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 0.5 * METER_PER_MILE, 0.5 * METER_PER_MILE);
//    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
}

- (NSString *)deviceLat {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.latitude];
}

- (NSString *)deviceLon {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.longitude];
}

- (NSString *)deviceAlt {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.altitude];
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - mapview delegate

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    
    
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    
    if (annotationView == nil)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
    }
    
    annotationView.canShowCallout = YES;
    
    //annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    annotationView.image = [UIImage imageNamed:@"icon_marker"];
    
    annotationView.annotation = annotation;
    
    [mapView.userLocation setTitle:@"My location here"];
    [mapView.userLocation
     setSubtitle:@"I am a subtitle"];
    
    return annotationView;
}

- (void) mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray<MKAnnotationView *> *)views {
    
    NSLog(@"pin taped");
    
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    NSLog(@"pin taped");
    
    [_detailView setHidden:NO];
    
    _detailView.alpha = 0;
    [UIView animateWithDuration:0.35f animations:^{
        
        _detailView.transform = CGAffineTransformMakeTranslation(0, -_detailView.frame.size.height);
//        _detailView.transform = CGAffineTransformMakeTranslation(0, 0);
        _detailView.alpha = 1;
    }];
}

- (void) mapView:(MKMapView *) mapview didDeselectAnnotationView:(nonnull MKAnnotationView *)view {
    
    NSLog(@"pin disappear");    
    
    _detailView.alpha = 1;
    [UIView animateWithDuration:0.35f animations:^{
        
        _detailView.transform = CGAffineTransformMakeTranslation(0, 0);
        _detailView.alpha = 0;
    }];
    
    //[_detailView dismiss];
}

- (void) mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    NSLog(@"pin taped");
    
}

#pragma mark - button actions

- (IBAction)callAction:(id)sender {

    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"15643313982"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}
- (IBAction)timeAction:(id)sender {
    
    [self showAlertDialog:@"No Data" message:@"" positive:@"OK" negative:nil];
    
}

@end
