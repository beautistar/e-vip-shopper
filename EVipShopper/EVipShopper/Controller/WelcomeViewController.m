//
//  WelcomeViewController.m
//  EVipShopper
//
//  Created by AOC on 03/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController () {
    
}

@property (weak, nonatomic) IBOutlet UIView *registerView;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;
@property (weak, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UITextField *tfWelcomeEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfRegisterEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfRegisterPwd;
@property (weak, nonatomic) IBOutlet UITextField *tfRegisterPwdCfm;
@property (weak, nonatomic) IBOutlet UITextField *tfLoginEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfLoginPwd;



@end

@implementation WelcomeViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    _lblWelcome.text = @"Welcome";
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    if([_tfLoginEmail isFirstResponder]) return 230;
    
    if ([_tfLoginPwd isFirstResponder]) return 170;
    
    if([_tfRegisterPwd isFirstResponder] || [_tfRegisterPwdCfm isFirstResponder]) return 200;
    
    return 250;
}

- (IBAction)firstNextAction:(id)sender {
    
    //_registerView.hidden = NO;
    _loginView.hidden = NO;
    [self showLoginView];
    _tfLoginEmail.text = _tfWelcomeEmail.text;
    
}


- (IBAction)closeAction:(id)sender {
    
    _registerView.hidden = YES;
}
- (IBAction)closeLoginAction:(id)sender {
    
    _loginView.hidden = YES;
    _lblWelcome.text = @"Welcome";
    
}

- (void) showLoginView {
    
    _lblWelcome.text = @"Log in";
    _loginView.hidden = NO;
}


@end
