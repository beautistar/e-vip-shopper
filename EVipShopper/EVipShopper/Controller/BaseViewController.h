//
//  BaseViewController.h
//  EVipShopper
//
//  Created by AOC on 03/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (void) showLoadingView;
- (void) showLoadingViewWithTitle:(NSString *) title;
- (void) hideLoadingView;
- (void) hideLoadingView : (NSTimeInterval) delay ;
- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative;
- (CGFloat) getOffsetYWhenShowKeybarod;

@end
