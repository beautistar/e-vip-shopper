//
//  RootViewController.h
//  EVipShopper
//
//  Created by AOC on 04/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface RootViewController : RESideMenu <RESideMenuDelegate>

@end
