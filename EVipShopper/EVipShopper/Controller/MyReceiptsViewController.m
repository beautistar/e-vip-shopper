//
//  MyReceiptsViewController.m
//  EVipShopper
//
//  Created by AOC on 04/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "MyReceiptsViewController.h"
#import "ReceiptCell.h"

@interface MyReceiptsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tblReceipt;
@end

@implementation MyReceiptsViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self initView];
}

- (void) initView {

    [self.navigationController setNavigationBarHidden:NO];
    [self.tabBarController.tabBar setHidden:NO];
    self.tabBarController.navigationController.navigationBar.hidden = NO;
}


#pragma mark -
#pragma mark - TableView datasource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReceiptCell *cell = (ReceiptCell *) [tableView dequeueReusableCellWithIdentifier:@"ReceiptCell"];
    return cell;   
}

@end
